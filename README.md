Construcción de imágenes

1. Servidor
   Ejecutar los siguientes comandos:
   cd servidor
   docker build -t servidor-irc .
2. Cliente
   Ejecutar los siguientes comandos:
   cd cliente
   docker build -t cliente-irc .

Crear contenedor:

1. Servidor:
   docker run -d --name servidor-irc servidor-irc
2. Cliente:
   docker run -it --rm --name cliente1 cliente-irc

Obtener ip del servidor:
docker inspect {id_contenedor}

Una vez en el contenedor de cliente hay que conectarse al servidor irc,
se deben ejecurtar los siguientes comandos:

1. /server add sv {ip_contenedor}/6667
2. /connect sv
3. /join #channel

Por cada cliente que desee ocupar el canal de texto debe ejecutar los comandos para
crear contenedor y conectarse al servidor.
